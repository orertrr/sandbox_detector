import redis
import time
import json
import jsonschema
import schema
import re
import os
from jsonschema import validate
from enum import IntEnum
from colorama import init, Fore

valueList = []

eNBList = []

abnormal = []

FinalResult = False

numOfComponent = 9

LOG_appmanager = "appmanager: "
LOG_submanager = "submanager: "
LOG_e2Manager_general = "e2Manager_general: "
LOG_e2Manager_addr = "e2Manager_address: "
LOG_e2Manager_eNB = "e2Manager_eNB: "
LOG_e2Manager_inst = "e2Manager_instance: "
LOG_e2Manager_Ran = "e2Manager_Ran: "
LOG_e2Manager_eNBaddr = "e2Manager_eNBaddr: "
LOG_UEID = "UEID: "


class OAICComponent(IntEnum):
    appmanager = 0
    submanager = 1
    e2Manager_general = 2
    e2Manager_addr = 3
    e2Manager_eNB = 4
    e2Manager_inst = 5
    e2Manager_Ran = 6
    e2Manager_eNBaddr = 7
    UEID = 8


def validateJsonSchema(jsonData, schema):
    try:
        validate(instance=jsonData, schema=schema)
        return True
    except jsonschema.ValidationError as err:
        return False

### Test Case ###

## Add your test here, you can validate some specify value in each component

def appmgr_test(val):
    result = validateJsonSchema(val, schema.appmgrSchema)
    if (result == False):
        abnormal[OAICComponent.appmanager] = True
    return result


def submgr_test(val):
    result = validateJsonSchema(val, schema.submgrSchema)
    if (result == False):
        abnormal[OAICComponent.submanager] = True
    return result


def e2M_general_test(val):
    result = validateJsonSchema(val, schema.e2generalSchema)
    if (result == False):
        abnormal[OAICComponent.e2Manager_general] = True
    return result


def e2M_addr_test(val):
    result = validateJsonSchema(val, schema.e2addrSchema)
    if (result == False):
        abnormal[OAICComponent.e2Manager_addr] = True
    return result


def e2M_eNB_test(val):
    result = validateJsonSchema(val, schema.e2eNBSchema)
    if (result == False):
        abnormal[OAICComponent.e2Manager_eNB] = True
    return result


def e2M_inst_test(val):
    result = validateJsonSchema(val, schema.e2InstanceSchema)
    if (result == False):
        abnormal[OAICComponent.e2Manager_inst] = True
    return result


def e2M_Ran_test(key, val):
    value = str(key[16:])
    value = value.replace(':', '')
    valueList.append(value)
    result = validateJsonSchema(val, schema.e2RanSchema)
    if (result == False):
        abnormal[OAICComponent.e2Manager_Ran] = True
    return result


def e2M_eNBaddr_test(key, val):
    value = str(key[16:])
    value = value.replace(':', '')
    valueList.append(value)
    result = validateJsonSchema(val, schema.e2eNBAddrSchema)
    if (result == False):
        abnormal[OAICComponent.e2Manager_eNBaddr] = True
    return result


def __matchSetOfeNB__():
    transList = []
    cpSet = set()

    for item in eNBList:
        s = item.encode('ascii')
        temp = s.split('\n')
        transList = transList + temp

    for item in transList:
        new_str = ''
        for c in item:
            if (ord(c) >= 32 and ord(c) <= 126):
                new_str += c
        cpSet.add(new_str)

    if ("" in cpSet):
        cpSet.remove("")

    for item in valueList:
        if (item in cpSet):
            cpSet.remove(item)

    if (len(cpSet) != 0):
        abnormal[OAICComponent.e2Manager_Ran] = True
        abnormal[OAICComponent.e2Manager_eNBaddr] = True
        abnormal[OAICComponent.e2Manager_eNB] = True
        print(valueList)
        print((cpSet))
    cpSet.clear()
### Test Case ###


def ueID_test(key, val):
    try:
        if (str(key) != val.get('UE ID')):
            abnormal[OAICComponent.UEID] = True
            return False
    except:
        abnormal[OAICComponent.UEID] = True
        return False
    result = validateJsonSchema(val, schema.ueJsonSchema)
    if (result == False):
        abnormal[OAICComponent.UEID] = True
    return result


def stringMatch(key, val):
    appmgr = re.compile(r'{appmgr}')
    submgr = re.compile(r'{submgr_e2SubsDb}')
    e2M_general = re.compile(r'{e2Manager},GENERAL')
    e2M_addr = re.compile(r'{e2Manager},E2TAddresses')
    e2M_eNB = re.compile(r'^{e2Manager},ENB$')
    e2M_inst = re.compile(r'{e2Manager},E2TInstance')
    e2M_Ran = re.compile(r'{e2Manager},RAN')
    e2M_eNBaddr = re.compile(r'{e2Manager},ENB:')
    ueID = re.compile(r'^[0-9]*$')
    if (appmgr.search(key) != None):
        return appmgr_test(val)
    if (submgr.search(key) != None):
        return submgr_test(val)
    if (e2M_general.search(key) != None):
        return e2M_general_test(val)
    if (e2M_addr.search(key) != None):
        return e2M_addr_test(val)
    if (e2M_eNB.search(key) != None):
        return e2M_eNB_test(val)
    if (e2M_inst.search(key) != None):
        return e2M_inst_test(val)
    if (e2M_Ran.search(key) != None):
        return e2M_Ran_test(key, val)
    if (e2M_eNBaddr.search(key) != None):
        return e2M_Ran_test(key, val)
    if (ueID.search(key) != None):
        return ueID_test(key, val)
    return None


def __abnormalLogShowing__():
    if (abnormal[OAICComponent.appmanager]):
        print(LOG_appmanager + Fore.RED + "Failed")
    else:
        print(LOG_appmanager + "Success")

    if (abnormal[OAICComponent.e2Manager_addr]):
        print(LOG_e2Manager_addr + Fore.RED + "Failed")
    else:
        print(LOG_e2Manager_addr + "Success")

    if (abnormal[OAICComponent.e2Manager_eNB]):
        print(LOG_e2Manager_eNB + Fore.RED + "Failed")
    else:
        print(LOG_e2Manager_eNB + "Success")

    if (abnormal[OAICComponent.e2Manager_eNBaddr]):
        print(LOG_e2Manager_eNBaddr + Fore.RED + "Failed")
    else:
        print(LOG_e2Manager_eNBaddr + "Success")

    if (abnormal[OAICComponent.e2Manager_general]):
        print(LOG_e2Manager_general + + Fore.RED + "Failed")
    else:
        print(LOG_e2Manager_general + "Success")

    if (abnormal[OAICComponent.e2Manager_inst]):
        print(LOG_e2Manager_inst + Fore.RED + "Failed")
    else:
        print(LOG_e2Manager_inst + "Success")

    if (abnormal[OAICComponent.e2Manager_Ran]):
        print(LOG_e2Manager_Ran + Fore.RED + "Failed")
    else:
        print(LOG_e2Manager_Ran + "Success")

    if (abnormal[OAICComponent.submanager]):
        print(LOG_submanager + Fore.RED + "Failed")
    else:
        print(LOG_submanager + "Success")
    if (abnormal[OAICComponent.UEID]):
        print(LOG_UEID + Fore.RED + "Failed")
    else:
        print(LOG_UEID + "Success")

    # redis ip should change when the env was changed
_redis = redis.Redis(host="10.244.0.27", port=6379,
                     db=0, decode_responses=True)
init(autoreset=True)
while True:
    # init state
    del abnormal[:]
    os.system('clear')
    
    for _ in range(numOfComponent):
        abnormal.append(False)

    keys = _redis.keys('*')
    # pull data from db
    for key in keys:
        type = _redis.type(key)
        if type == "string":
            val = _redis.get(key)
            if val[0] == '{':
                val = json.loads(val)
            if (stringMatch(key, val) == False or stringMatch(key, val) == None):
                # show which key and val crashed or not matched
                print(key)
                print(val)
                FinalResult = True
        if type == "set":
            _set = _redis.smembers(key)
            for val in _set:
                if (stringMatch(key, val) == False or stringMatch(key, val) == False):
                    # show which key and val crashed or not matched
                    print(key)
                    print(val)
                    FinalResult = True
            eNBList = list(_set)

    __matchSetOfeNB__()
    __abnormalLogShowing__()
    if (FinalResult):
        print("Result Of this Env: " + Fore.RED + "Failed")
    else:
        print("Result Of this Env: Success")
    time.sleep(0.5)
